<?php
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;

Route::get('/table', [TableController::class, 'table']);
Route::get('/data-tables', [TableController::class, 'dataTables']);
Route::resource('cast', CastController::class);
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">AdminLTE</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/table') }}" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Table</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/data-tables') }}" class="nav-link">
                        <i class="nav-icon fas fa-database"></i>
                        <p>Data Tables</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>

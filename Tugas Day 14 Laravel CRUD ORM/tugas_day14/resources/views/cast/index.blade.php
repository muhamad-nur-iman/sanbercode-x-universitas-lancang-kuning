@extends('layouts.master')

@section('title', 'Casts')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <h2 class="mb-4">Casts List</h2>
            <a href="{{ route('cast.create') }}" class="btn btn-success mb-3">Add New Cast</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Bio</th>
                        <th>Birth Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($casts as $cast)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $cast->name }}</td>
                        <td>{{ $cast->bio }}</td>
                        <td>{{ $cast->birth_date }}</td>
                        <td>
                            <a href="{{ route('cast.show', $cast->id) }}" class="btn btn-info">Show</a>
                            <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('cast.destroy', $cast->id) }}" method="POST" style="display:inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

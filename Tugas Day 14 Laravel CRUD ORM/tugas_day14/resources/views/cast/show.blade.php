@extends('layouts.master')

@section('title', 'Show Cast')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h2 class="mb-4">Cast Details</h2>
            <div class="card">
                <div class="card-header">
                    <h3>{{ $cast->name }}</h3>
                </div>
                <div class="card-body">
                    <p><strong>Bio:</strong> {{ $cast->bio }}</p>
                    <p><strong>Birth Date:</strong> {{ $cast->birth_date }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

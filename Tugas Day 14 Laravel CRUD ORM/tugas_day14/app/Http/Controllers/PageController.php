<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function table()
    {
        return view('table');
    }

    public function dataTables()
    {
        return view('data-tables');
    }
}

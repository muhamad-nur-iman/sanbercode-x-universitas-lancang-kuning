<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    // Menampilkan list data para pemain film
    public function index()
    {
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }

    // Menampilkan form untuk membuat data pemain film baru
    public function create()
    {
        return view('cast.create');
    }

    // Menyimpan data baru ke tabel Cast
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'bio' => 'nullable',
            'birth_date' => 'required|date',
        ]);

        Cast::create($request->all());
        return redirect()->route('cast.index')->with('success', 'Cast created successfully.');
    }

    // Menampilkan detail data pemain film dengan id tertentu
    public function show(Cast $cast)
    {
        return view('cast.show', compact('cast'));
    }

    // Menampilkan form untuk edit pemain film dengan id tertentu
    public function edit(Cast $cast)
    {
        return view('cast.edit', compact('cast'));
    }

    // Menyimpan perubahan data pemain film (update) untuk id tertentu
    public function update(Request $request, Cast $cast)
    {
        $request->validate([
            'name' => 'required',
            'bio' => 'nullable',
            'birth_date' => 'required|date',
        ]);

        $cast->update($request->all());
        return redirect()->route('cast.index')->with('success', 'Cast updated successfully.');
    }

    // Menghapus data pemain film dengan id tertentu
    public function destroy(Cast $cast)
    {
        $cast->delete();
        return redirect()->route('cast.index')->with('success', 'Cast deleted successfully.');
    }
}

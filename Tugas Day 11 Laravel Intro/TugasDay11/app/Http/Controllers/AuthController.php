<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showRegisterForm()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $first_name = $request->input('firstname');
        $last_name = $request->input('lastname');

        return view('welcome', compact('first_name', 'last_name'));
    }
}

1. Membuat Database

CREATE DATABASE library;

2. Membuat Tabel di Dalam Database

CREATE TABLE users (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255)
);

CREATE TABLE categories (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE books (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    summary TEXT,
    stock INTEGER,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

3. Memasukkan Data ke dalam Tabel

INSERT INTO users (name, email, password) VALUES
('Nama_peserta', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO categories (name) VALUES
('Novel'),
('Manga'),
('Comic'),
('History'),
('IT');

INSERT INTO books (title, summary, stock, category_id) VALUES
('One Piece', 'The series focuses on Monkey D Luffy, a young man made of rubber', 50, 2),
('Laskar Pelangi', 'Belitung is known for its richness in tin, making it one of Indonesia\'s richest islands', 25, 1),
('Your Name', 'Mitsuha Miyamizu, a high school girl living in the fictional town of Itomori in Gifu Prefecture\'s', 15, 2);

4. Mengambil Data dari Database

a. Mengambil data users

SELECT id, name, email FROM users;

b. Mengambil data books

SELECT * FROM books WHERE stock > 20;

SELECT * FROM books WHERE title LIKE '%one%' OR title LIKE '%pela%' OR title LIKE '%ame%';

c. Menampilkan data items books dengan categories

SELECT books.title, books.summary, books.stock, books.category_id, categories.name AS category 
FROM books 
JOIN categories 
ON books.category_id = categories.id;

5. Mengubah Data dari Database

UPDATE books SET stock = 30 WHERE title = 'One Piece';

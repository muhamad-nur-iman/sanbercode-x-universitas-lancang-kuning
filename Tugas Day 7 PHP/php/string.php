<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
 /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!";
        $second_sentence = "I'm ready for the challenges";

        // Menghitung panjang string
        $panjang_first_sentence = strlen($first_sentence);
        $panjang_second_sentence = strlen($second_sentence);

        // Menghitung jumlah kata
        $jumlah_kata_first_sentence = str_word_count($first_sentence);
        $jumlah_kata_second_sentence = str_word_count($second_sentence);

        echo "Panjang string: $panjang_first_sentence, Jumlah kata: $jumlah_kata_first_sentence <br>";
        echo "Panjang string: $panjang_second_sentence, Jumlah kata: $jumlah_kata_second_sentence <br>";

        echo "<h3> Soal No 2</h3>";

        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */

        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        // Mengambil kata pertama
        $kata_pertama = strtok($string2, " ");
        echo "Kata pertama: $kata_pertama <br>";
        
        // Mengambil kata kedua
        $kata_kedua = strtok(" ");
        echo "Kata kedua: $kata_kedua <br>";

        // Mengambil kata ketiga
        $kata_ketiga = strtok(" ");
        echo "Kata ketiga: $kata_ketiga <br>";

        echo "<h3> Soal No 3 </h3>";

        

        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" <br>"; 
        // Mengganti kata "sexy" dengan "awesome"
        $string3_baru = str_replace("sexy", "awesome", $string3);
        echo "String baru: \"$string3_baru\" ";
    ?>
</body>
</html>

<?php
use App\Http\Controllers\PageController;

Route::get('/table', [PageController::class, 'table']);
Route::get('/data-tables', [PageController::class, 'dataTables']);

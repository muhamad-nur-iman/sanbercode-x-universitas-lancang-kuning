// Soal Function No. 1

// Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai "Halo Sanbers!" yang kemudian dapat ditampilkan di console.

// function teriak() {
  //your code
// }

// TEST CASE
// console.log(teriak()) // "Halo Sanbers!" 

// Jawaban Soal No.1

function teriak() {
    return "Halo Sanbers!";
  }
  
  console.log(teriak()); // "Halo Sanbers!"
  
// Soal Function No. 2

// Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter

// function kalikan(num1, num2) {
    // your code
//  }
  
// console.log(kalikan(4, 12)) // 48

// Jawaban Soal No.2

function kalikan(num1, num2) {
    return num1 * num2;
  }
  
  console.log(kalikan(4, 12)); // 48

// Soal Function No. 3

// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

// function introduce(name, age, address, hobby) {
    // your code
// }
  
   
// TEST CASES
// console.log(introduce("Agus", 30, "Jogja", "Gaming")) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jogja, 
// dan saya punya hobby yaitu Gaming!" 

// Jawaban Soal No.3

function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
  }
  
  console.log(introduce("Iman", 22, "Jawa Timur", "Gaming"));
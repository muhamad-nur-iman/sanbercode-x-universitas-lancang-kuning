
// Soal String
// Soal No.1 (Membuat kalimat),
// Terdapat kumpulan variable dengan data string sebagai berikut

// var word = 'JavaScript'; 
// var second = 'is'; 
// var third = 'awesome'; 
// var fourth = 'and'; 
// var fifth = 'I'; 
// var sixth = 'love'; 
// var seventh = 'it!';

// Buatlah agar kata-kata di atas menjadi satu kalimat . Output:
// JavaScript is awesome and I love it! 

// Jawaban Soal No.1

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var sentence = word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh;
console.log(sentence);

// Soal No.2 Mengurai kalimat (Akses karakter dalam string),
// Terdapat satu kalimat seperti berikut:

// var sentence = "I am going to be Web Developer"; 

// var exampleFirstWord = sentence[0] ; 
// var secondWord = sentence[2] + sentence[3]  ; 
// var thirdWord; // lakukan sendiri 
// var fourthWord; // lakukan sendiri 
// var fifthWord; // lakukan sendiri 
// var sixthWord; // lakukan sendiri 
// var seventhWord; // lakukan sendiri 

// console.log('First Word: ' + exampleFirstWord); 
// console.log('Second Word: ' + secondWord); 
// console.log('Third Word: ' + thirdWord); 
// console.log('Fourth Word: ' + fourthWord); 
// console.log('Fifth Word: ' + fifthWord); 
// console.log('Sixth Word: ' + sixthWord); 
// console.log('Seventh Word: ' + seventhWord); 

// Buat menjadi Output berikut:

// First word: I 
// Second word: am 
// Third word: going 
// Fourth word: to 
// Fifth word: be 
// Sixth word: Web 
// Eighth word: Developer

// Jawaban Soal No.2

var sentence = "I am going to be Web Developer"; 

var exampleFirstWord = sentence.substring(0, 1); 
var secondWord = sentence.substring(2, 4); 
var thirdWord = sentence.substring(5, 10); 
var fourthWord = sentence.substring(11, 13); 
var fifthWord = sentence.substring(14, 16); 
var sixthWord = sentence.substring(17, 20); 
var seventhWord = sentence.substring(21, sentence.length); 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord);

// Soal No.3 Mengurai Kalimat dan Menentukan Panjang String

// var sentence3 = 'wow JavaScript is so cool'; 

// var exampleFirstWord3 = sentence3.substring(0, 3); 
// var secondWord3; // do your own! 
// var thirdWord3; // do your own! 
// var fourthWord3; // do your own! 
// var fifthWord3; // do your own! 

// var firstWordLength = exampleFirstWord3.length  
// lanjutkan buat variable lagi di bawah ini 
// console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
// console.log('Second Word: ' + secondWord3); 
// console.log('Third Word: ' + thirdWord3); 
// console.log('Fourth Word: ' + fourthWord3); 
// console.log('Fifth Word: ' + fifthWord3); 

// Output:

// First Word: wow, with length: 3 
// Second Word: JavaScript, with length: 10 
// Third Word: is, with length: 2 
// Fourth Word: so, with length: 2 
// Fifth Word: cool, with length: 4

// Jawaban Soal No.3

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21, 25); 

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);